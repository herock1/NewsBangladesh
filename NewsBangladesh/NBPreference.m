//
//  NBPreference.m
//  NewsBangladesh
//
//  Created by Ishtiak Ahmed(Nidaan Systems Ltd) on 3/16/15.
//  Copyright (c) 2015 DGDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NBPreference.h"

static NSString * const kNBFONTSIZEKEY = @"NBFONTSIZEKEY";
static NSString * const kNBLANGUAGEMODEEKEY = @"NBLANGUAGEMODEEKEY";
static NSString * const kNBBREAKINGNEWSALERTEEKEY = @"NBBREAKINGNEWSALERTEEKEY";
static NSString * const kNBBAUTOMATICREFRESHKEY = @"NBBAUTOMATICREFRESHKEY";

static NSString * const kNBGlobalHeadingFontFamily = @"HelveticaNeue-Bold";
static NSString * const kNBGlobalBodyFontFamily = @"HelveticaNeue";

@interface NBPreference ()

@end

@implementation NBPreference

+ (void)setFontSize:(CGFloat)fontSize {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setFloat:fontSize forKey:kNBFONTSIZEKEY];
    [pref synchronize];
}

+ (CGFloat)fontSize {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    
    CGFloat fontSize = [pref floatForKey:kNBFONTSIZEKEY];
    
    return (fontSize <= 5.0f) ? 16.0f : fontSize;
}

+ (UIFont *)fontForHeadingLabel {
    
    return [UIFont fontWithName:kNBGlobalHeadingFontFamily size:[NBPreference fontSize] + 2.0f];
}

+ (UIFont *)fontForBodyLabel {
    
    return [UIFont fontWithName:kNBGlobalBodyFontFamily size:[NBPreference fontSize]];
}

+ (void)setLanguageMode:(NBLanguageMode) mode {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setInteger:mode forKey:kNBLANGUAGEMODEEKEY];
    [pref synchronize];
}

+ (NBLanguageMode)languageMode {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    
    return (NBLanguageMode)[pref integerForKey:kNBLANGUAGEMODEEKEY];
}

+ (void)setBreakingNewsAlert:(BOOL)yes {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setBool:yes forKey:kNBBREAKINGNEWSALERTEEKEY];
    [pref synchronize];
}

+ (BOOL)breakingNewsAlert {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    
    return [pref boolForKey:kNBBREAKINGNEWSALERTEEKEY];
}

+ (void)setAutomaticRefresh:(BOOL)yes {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setBool:yes forKey:kNBBAUTOMATICREFRESHKEY];
    [pref synchronize];
}

+ (BOOL)automaticRefresh {
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    
    return [pref boolForKey:kNBBAUTOMATICREFRESHKEY];
}
+ (NSString *)deviceUUID
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:[[NSBundle mainBundle] bundleIdentifier]])
        return [[NSUserDefaults standardUserDefaults] objectForKey:[[NSBundle mainBundle] bundleIdentifier]];
    
    @autoreleasepool {
        
        CFUUIDRef uuidReference = CFUUIDCreate(nil);
        CFStringRef stringReference = CFUUIDCreateString(nil, uuidReference);
        NSString *uuidString = (__bridge NSString *)(stringReference);
        [[NSUserDefaults standardUserDefaults] setObject:uuidString forKey:[[NSBundle mainBundle] bundleIdentifier]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        CFRelease(uuidReference);
        CFRelease(stringReference);
        return uuidString;
    }
}

@end
